﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
var flag;
var user;
var currentUserId;
var curBalance;
(function () {
    "use strict";

    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);

        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
        user = window.localStorage.getItem("currentUser");
        var db = openDatabase('seQRDatabase', '1.0', 'seQRDatabase', '2*1024*1024');
        db.transaction(function (tx) {
            tx.executeSql('SELECT userid FROM USERS WHERE email =\"' + user + '\"', [], function (tx, results) {
                var len = results.rows.length, i;
                for (i = 0; i < len; i++) {
                    currentUserId = results.rows.item(i).userid;
                }

                tx.executeSql('SELECT * FROM CARDS WHERE userid=\"' + currentUserId + '\"', [], function (tx, results) {

                   var len3 = results.rows.length;

                    if (len3 === 0) {
                        flag = true;
                    }
                    else {
                        flag = false;
                    }

                });
                tx.executeSql('SELECT balance FROM USERS WHERE userid=\"' + currentUserId + '\"', [], function (tx, results) {
                    var length = results.rows.length,i;
                    for(i=0;i<length;i++)
                    {
                        curBalance = results.rows.item(i).balance;
                    }
                });
            });
        });
    };



    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };


})();

// functions here are defined to be run after an event happens, mostly onclick events

// these two functions process a top-up amount, they were one anonymous function, but unsure if they would work, cannot test on PC, need real device for testing out
function topUp() {
    // taking from a pop-up box a value to top up by, then converting the stored string to a double and adding to oldbalance to get new topped-up balance
    navigator.notification.alert("Your account balance is: €" + curBalance); // tells the user their current balance
    navigator.notification.prompt('Enter in amount to top-up by:', doTopUp, "Top-up",["Confirm Top-Up","Cancel Top-Up"]);
};
function doTopUp(amount) {
    var db = openDatabase('seQRDatabase', '1.0', 'seQRDatabase', '2*1024*1024');
    
    db.transaction(function (tx) {
        tx.executeSql('SELECT balance FROM USERS WHERE userid=\"' + currentUserId + '\"', [], function (tx, results) {
            var len = results.rows.length, i;
            for(i = 0;i<len;i++)
            {
                var previousBalance = results.rows.item(i).balance;
            }
            if (parseFloat(amount.input1) > 0) {
                //********need to check if the card added*************
                if (flag === false)
                {
                    var newBalance = parseFloat(previousBalance) + parseFloat(amount.input1);

                    var date = new Date();
                    var dd = date.getDate();
                    var mm = date.getMonth() + 1;
                    var yyyy = date.getFullYear();

                    if (dd < 10) {
                        dd = '0' + dd
                    }

                    if (mm < 10) {
                        mm = '0' + mm
                    }
                    date = dd + '/' + mm + '/' + yyyy;
                    var newid = (new Date().getMilliseconds()).toString();
                    var transactionName = "Top-Up on Account";
                    var amountToStore = amount.input1.toString();


                    tx.executeSql('UPDATE USERS SET balance=\"' + newBalance + '\" WHERE userid=\"' + currentUserId + '\"');
                    tx.executeSql('INSERT INTO TRANSACTIONS(transactionid,userid,date,name,price) VALUES(?,?,?,?,?)', [newid, currentUserId, date, transactionName, amountToStore]);



                    navigator.notification.alert("Your new balance: " + newBalance);
                }
                else
                {
                    navigator.notification.alert("sorry, you do not have a card set on this account to top up with, please enter a valid card to your account first to top-up");
                }
                
            }
            else
            {
                navigator.notification.alert("Please Enter a valid amount to top-up by");
            }
        });
    });
}
// adds balance chosen on confirming top-up to add to current blance
function processTopUp(amount) {
    if (parseFloat(amount.input1) > 0)
    {
        if (window.localStorage.getItem("card:" + window.localStorage.getItem("currentUser")) !== null)
        {

            // rounding the number down to lowest of two decimal places for cents of top-up amount, auto rounds up when greater than 5 on third decimal place, not controllable         
            var value = +(parseFloat(amount.input1).toFixed(2));
            var balance = +(parseFloat(localStorage.getItem("balance")).toFixed(2)) + value;

            // fails to work on repeated inputs above 10 in size for top-ups
            //var value = Number(Math.floor(parseFloat(amount.input1) + 'e2') + 'e-2');
            // exponent of decimal places of 2 works but cannot stop input breaking when user enters more than 2 decimal places for a float value
            //var balance = Number(Math.floor(parseFloat(localStorage.getItem("balance")) + 'e2') + 'e-2') + value;
            localStorage.setItem("balance", balance.toString());
            navigator.notification.alert("Your new account balance is : €" + localStorage.getItem("balance"));

        }
        else {
            navigator.notification.alert("sorry, you do not have a card set on this account to top up with, please enter a valid card to your account first to top-up");
        }
       
    }
    else
    {
        navigator.notification.alert("Please Enter a valid amount to top-up by");
    }
    
   
};

function logOut()
{
    // placing user back into their record entry with new balance changes since logging into the application
    
    window.localStorage.removeItem("currentUser");
    
    window.location.href = "../index.html";
}