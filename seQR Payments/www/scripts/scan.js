﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.

// global price and name variable for storing transactions and altering the users balance
var price;
var name;
var curBalance;
var user
var currentUserId;

(function () {
    "use strict";
    
    
    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);

        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
       
        // part of API text from BarCodeScanner available at https://github.com/phonegap/phonegap-plugin-barcodescanner
        //var balance = window.localStorage.getItem("balance");
        user = window.localStorage.getItem("currentUser");
        
        var db = openDatabase('seQRDatabase', '1.0', 'seQRDatabase', '2*1024*1024');
        db.transaction(function (tx) {
            tx.executeSql('SELECT userid FROM USERS WHERE email =\"' + user + '\"', [], function (tx, results) {
                var len = results.rows.length, i;
                for (i = 0; i < len; i++) {
                    currentUserId = results.rows.item(i).userid;
                }

                
                tx.executeSql('SELECT balance FROM USERS WHERE userid=\"' + currentUserId + '\"', [], function (tx, results) {
                    var length = results.rows.length, i;
                    for (i = 0; i < length; i++) {
                        curBalance = results.rows.item(i).balance;
                    }
                    
                    navigator.notification.alert("Your account balance is €" + curBalance);
                });
                
                
            });
        });
        
        
        cordova.plugins.barcodeScanner.scan(
    function (result) {
        if (!result.cancelled) {
            // In this case we only want to process QR Codes
            if (result.format == "QR_CODE") {
                //"Paying:Kingston 32gb USB Flash Drive:25"
                //"Recieving:Vitaliy:34"
                var action = result.text.substr(0, result.text.indexOf(":"));
                name = result.text.substring(result.text.indexOf(":") + 1, result.text.lastIndexOf(":"));
                price = result.text.substr(result.text.lastIndexOf(":") + 1);
                

                // check for enough money, paying code or recieving QR code and valid format of QR code
                if (parseFloat(price) > parseFloat(curBalance) && action == "Paying")
                {
                    navigator.notification.alert("Sorry, you don't have enough money in your account to pay €" + price + " with your account balance at €" + curBalance);
                    window.location.href = "mainMenu.html";
                }
                else if (action == "Paying")
                {
                    navigator.notification.confirm("Confirm " + action + " €" + price + " for " + name,transferPay,"Confirm Payment",["Confirm Sending","Cancel Sending"]);
                }
                else if(action == "Recieving")
                {
                    navigator.notification.confirm("Confirm " + action + " €" + price + " from " + name,transferRecieve, "Confirm Recieving", ["Confirm Recieving", "Cancel Recieving"]);
                }
                else
                {
                    navigator.notification.alert("Action is not define Correctly, invalid QR Code Format");
                }
                

                // This is the retrieved content of the qr code
                //console.log(value);
            } else {
                alert("Sorry, only qr codes this time ;)");
            }
        } else {
            alert("The user has dismissed the scan");
        }
    },
        function (error) {
            alert("An error ocurred: " + error);
        }
    );

    };


    function transferPay(buttonIndex)
    {
        
        if (buttonIndex == 1)
        {
            //var balance = +(parseFloat(window.localStorage.getItem("balance")).toFixed(2));
            
            var newBalance =parseFloat(curBalance) - parseFloat(price);
            //window.localStorage.setItem("balance", newBalance.toString());
            // storing payment transaction
            //var user = window.localStorage.getItem("currentUser");
            //var transactions = window.localStorage.getItem("transactions:" + user);
            var date = todayDate();
            //window.localStorage.setItem("transactions:" + user, transactions + ":" + date + ":" + name + ":-" + price + ":");
            var db = openDatabase('seQRDatabase', '1.0', 'seQRDatabase', '2*1024*1024');
            var newid = (new Date().getMilliseconds()).toString();
            var priceToStore = "-" + price.toString();
            db.transaction(function (tx) {
                tx.executeSql('UPDATE USERS SET balance=\"' + newBalance + '\" WHERE userid=\"' + currentUserId + '\"');
                tx.executeSql('INSERT INTO TRANSACTIONS(transactionid,userid,date,name,price) VALUES(?,?,?,?,?)', [newid, currentUserId, date, name, priceToStore]);
            });
            navigator.notification.alert("Payment made of €" + price + " your new balance is €" + newBalance);
        }
    }

    function transferRecieve(buttonIndex) {

        if (buttonIndex == 1) {
            
            var newBalance = parseFloat(curBalance) + parseFloat(price);
            //window.localStorage.setItem("balance", newBalance.toString());
            // storing recieve transaction
            //var user = window.localStorage.getItem("currentUser");
            //var transactions = window.localStorage.getItem("transactions:" + user);
            var date = todayDate();
            //window.localStorage.setItem("transactions:" + user, transactions + ":" + date + ":" + name + ":" + price + ":");
            var db = openDatabase('seQRDatabase', '1.0', 'seQRDatabase', '2*1024*1024');
            var newid = (new Date().getMilliseconds()).toString();
            db.transaction(function (tx) {
                tx.executeSql('UPDATE USERS SET balance=\"' + newBalance + '\" WHERE userid=\"' + currentUserId + '\"');
                tx.executeSql('INSERT INTO TRANSACTIONS(transactionid,userid,date,name,price) VALUES(?,?,?,?,?)', [newid, currentUserId, date, name, price]);
            });
            navigator.notification.alert("Payment made of €" + price + " your new balance is €" + newBalance);

        }

    }

    function todayDate()
    {
        var date = new Date();
        var dd = date.getDate();
        var mm = date.getMonth() + 1;
        var yyyy = date.getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        date = dd + '/' + mm + '/' + yyyy;
        return date;
    }


    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };


})();

/*
cordova.plugins.barcodeScanner.scan(
        function (result) {
            if (!result.cancelled) {
                // In this case we only want to process QR Codes
                if (result.format == "QR_CODE") {
                    var value = result.text;
                    // This is the retrieved content of the qr code
                    console.log(value);
                } else {
                    alert("Sorry, only qr codes this time ;)");
                }
            } else {
                alert("The user has dismissed the scan");
            }
        },
            function (error) {
                alert("An error ocurred: " + error);
            }
        );
*/