﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.

// global variable to user processing for card storing and deleting
var user;
var currentUserId;
var len3;
var flag;
(function () {
    "use strict";

    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);


        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
        // get user out for card processing
        user = window.localStorage.getItem("currentUser");
        //get current user id
        var db = openDatabase('seQRDatabase', '1.0', 'seQRDatabase', '2*1024*1024');
        db.transaction(function (tx) {
            tx.executeSql('SELECT userid FROM USERS WHERE email =\"' + user + '\"', [], function (tx, results) {
                var len = results.rows.length, i;
                for (i = 0; i < len; i++) {
                    currentUserId = results.rows.item(i).userid;
                }
                
                tx.executeSql('SELECT * FROM CARDS WHERE userid=\"' + currentUserId + '\"', [], function (tx, results) {
                    
                    len3 = results.rows.length;
                    
                    if(len3 === 0)
                    {
                        flag = true;
                    }
                    else {
                        flag = false;
                    }
                    
                });
            });
        });

    };


    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };


})();






function removeCard() {
    var db = openDatabase('seQRDatabase', '1.0', 'seQRDatabase', '2*1024*1024');    
    
   
    if (flag===false)
    {
        // remove the credit card here and notify the user of the removal
        //window.localStorage.removeItem("card:" + user);
        
        db.transaction(function (tx) {
            tx.executeSql('DELETE FROM CARDS WHERE userid=\"' + currentUserId + '\" ');
        });
        navigator.notification.alert("Current Card has been removed from your account");
        flag = true;
    }
    else {
        // annouce to the user there is no card to remove
        navigator.notification.alert("There is no Current Card to remove from your account");
    }


};

// store card with the user
function storeCard() {
    var db = openDatabase('seQRDatabase', '1.0', 'seQRDatabase', '2*1024*1024');
    var cardNumber = document.getElementsByName("cardNumber")[0].value;
    var month = document.getElementsByName("month")[0].value;
    var year = document.getElementsByName("year")[0].value;
    var cvc = document.getElementsByName("cvc")[0].value;
    var newid = (new Date().getMilliseconds()).toString();
    
    if(flag===true)
    {
        
        
        db.transaction(function (tx) {
            tx.executeSql('INSERT INTO CARDS (cardid,userid,cardnumber,year,month,cvc) VALUES(?,?,?,?,?,?)', [newid, currentUserId, cardNumber, year, month, cvc]);
            
        });

        navigator.notification.alert("Your new card has been successfully added");
    }
    else
    {
        navigator.notification.alert("Please remove your current card to add a new card");
    }
    
   


   
   
        

}