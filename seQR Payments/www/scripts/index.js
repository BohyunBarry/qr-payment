﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.

var securityCode;
var count = 0;

(function () {
    "use strict";

    document.addEventListener( 'deviceready', onDeviceReady.bind( this ), false );

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener( 'pause', onPause.bind( this ), false );
        document.addEventListener('resume', onResume.bind(this), false);
        
        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
        var parentElement = document.getElementById('deviceready');
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');
        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');
        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.

        



        // using localStorage API included in Cordova, available at: https://cordova.apache.org/docs/en/latest/cordova/storage/storage.html
        // you create Key/Value pairs for storage and can get the value out along with the key, the value can be stringified to store an object or other data type
        // setting up the database values to exists within the application, giving users a free starting credit of 20 euros on first start up of the application, later will have login accounts

        // setting up user accounts with balances if not already set, model and sample data with transaction support, implemented in register new user as well:
        
        // old local storage version of startup database
        //if (window.localStorage.getItem("vitaliy@student.dkit.ie") == null)
        //{
        //    window.localStorage.setItem("vitaliy@student.dkit.ie", "Vitaliy:Cars123@:50");
        //    window.localStorage.setItem("kieronpeters1@gmail.com", "Kieron:Jackie111@:25");
        //    window.localStorage.setItem("transactions:kieronpeters1@gmail.com", "");
        //    window.localStorage.setItem("transactions:vitaliy@student.dkit.ie", "");
        //}

    };


    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };


})();

// checks account exists first and then if matching password, lets the user into the application, logout will store the balance the back into the user account
function validateUser()
{
    
    //var email = document.getElementsByName("email")[0].value;
    //var password = document.getElementsByName("password")[0].value;
    //var account = window.localStorage.getItem(email);
    var db = openDatabase('seQRDatabase', '1.0', 'seQRDatabase', '2*1024*1024');




    var email = document.getElementsByName("email")[0].value;
    var password = document.getElementsByName("password")[0].value;
    var checkpassword;
    var checkbalance;
    //var account = window.localStorage.getItem(email);
    db.transaction(function (tx) {
        tx.executeSql('SELECT balance,password FROM USERS WHERE email=\"' + email + '" ', [], function (tx, results) {
            var len = results.rows.length, i;
            
            if (len < 1) {
                document.getElementById("Error").innerHTML = "Sorry, your entered information does not match our records, please try again or register";
                
            }
            else {
                
                for (i = 0; i < len; i++) {
                    checkpassword = results.rows.item(i).password;
                    checkbalance = results.rows.item(i).balance;
                }
                if (password == checkpassword) {
                    //window.localStorage.setItem("balance", checkbalance);
                    window.localStorage.setItem("currentUser", email);

                    // enter security code to be sent via SMS to user phone, won't generate if failed 5 times already to log in
                    if (count > 4)
                    {
                        confirmClosed();
                    }
                    else
                    {
                        securityCode = Math.floor(Math.random() * 8999) + 1000;
                        navigator.notification.beep(2);
                        navigator.notification.confirm("Security Code: " + securityCode, confirmClosed, "seQR Payments", "Close");
                    }

                    
                }
                else {
                    document.getElementById("Error").innerHTML = "Sorry, your entered information does not match the Username or Password";
                }
            }

        });

    });


    //if (account !== null)
    //{
        

    //    if (password == account.substring(account.indexOf(":") + 1,account.lastIndexOf(":")))
    //    {
    //        // enter security code to be sent via SMS to user phone
    //        securityCode = Math.floor(Math.random() * 8999) + 1000;

    //        navigator.notification.beep(2);
    //        navigator.notification.confirm("Security Code: " + securityCode, confirmClosed, "seQR Payments", "Close");
    //        // supposed to send text message to defined number
    //        var smsplugin = cordova.require("info.asankan.phonegap.smsplugin.smsplugin");
    //        smsplugin.send(00353861573492, "Security Code: " + securityCode);
    //        window.localStorage.setItem("balance", account.substr(account.lastIndexOf(":") + 1));
    //        window.localStorage.setItem("currentUser", email); 
            
    //    }
    //    else
    //    {
    //        document.getElementById("Error").innerHTML = "Sorry, your entered information does not match the Username or Password";
    //    }
        
    //}
    //else
    //{
    //    document.getElementById("Error").innerHTML = "Sorry, your entered information does not match our records, please try again or register";
    //}


}

function confirmClosed()
{
    if (count > 4)
    {
        navigator.notification.alert("Sorry, account deactivated after 5 failed attempts, go to your registed email account to reactivate your account to log in again");
    }
    else
    {
        navigator.notification.prompt('Hello' + window.localStorage.getItem("currentUser") + ', Enter in Phone Text Security Code:', processLogIn, "Security Code", ["Confirm log-in", "Cancel log-in"]);
    }
    
}

function processLogIn(code) {
    if (parseInt(code.input1) == securityCode)
    {
        window.location.href = 'view/mainMenu.html';
        count = 0;
    }
    else
    {
        navigator.notification.alert("Invalid Code, Please try again");
        count++;
    }
};





/*
function scanning() {
    scan(
function (result) {
    if (!result.cancelled) {
        // In this case we only want to process QR Codes
        if (result.format == "QR_CODE") {
            var value = result.text;
            // This is the retrieved content of the qr code
            console.log(value);
        } else {
            alert("Sorry, only qr codes this time ;)");
        }
    } else {
        alert("The user has dismissed the scan");
    }
},
    function (error) {
        alert("An error ocurred: " + error);
    }
);
};
*/


/*
var pictureSource;
var destinationType;
pictureSource = navigator.camera.PictureSourceType;
destinationType = navigator.camera.DestinationType;
navigator.camera.getPicture(onPhotoDataSuccess, onFail, {
    quality: 50,
    destinationType: destinationType.DATA_URL
});
*/