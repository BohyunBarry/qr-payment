﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.

var email;
var username;
var curpassword;
var password;
var cpassword;
var balance;


(function () {
    "use strict";

    
    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);

        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
        
        email = document.getElementsByName("email")[0];
        username = document.getElementsByName("username")[0];
        curpassword = document.getElementsByName("curpassword")[0];
        balance = document.getElementsByName("balance")[0];
        password = document.getElementsByName("password")[0];
        cpassword = document.getElementsByName("cpassword")[0];

        

        var userEmail = window.localStorage.getItem("currentUser");
        var data = window.localStorage.getItem(userEmail);
        email.value = userEmail;
        var db = openDatabase('seQRDatabase', '1.0', 'seQRDatabase', '2*1024*1024');
        
        db.transaction(function (tx) {            
            tx.executeSql('SELECT * FROM USERS WHERE email=\"' + userEmail + '\"', [], function (tx, results) {
                
                var len = results.rows.length, i;
                for (i = 0; i < len; i++)
                {
                    var u = results.rows.item(i).username;
                    var cp = results.rows.item(i).password;
                    var b = results.rows.item(i).balance;
                }
                username.value = u;
                curpassword.value = cp;
                balance.value = b;
            });
        },null);
        //username.value = data.substring(0, data.indexOf(":"));
        //curpassword.value = data.substring(data.indexOf(":") + 1, data.lastIndexOf(":"));
        //balance.value = data.substr(data.lastIndexOf(":") + 1);



    };


    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };


})();

function changePassword() {
    var db = openDatabase('seQRDatabase', '1.0', 'seQRDatabase', '2*1024*1024');
    var password1 = document.getElementsByName("password")[0].value;
    alert(password1);
    db.transaction(function (tx) {
        alert(password1);
        alert(email.value);
        tx.executeSql('UPDATE USERS SET password=\"' + password1 + '\" WHERE email=\"' + email.value + '\" ');
        alert("query ok");
    });
    //window.localStorage.setItem(email.value, username.value + ":" + password.value + ":" + balance.value);
    navigator.notification.alert("Congrats, " + username.value + " you have changed your password successfully!");

};


// Validation code from: 
// http://codepen.io/diegoleme/pen/surIK

function validatePassword() {

    var password = document.getElementsByName("password")[0];
    var cpassword = document.getElementsByName("cpassword")[0];

    if (password.value != cpassword.value) {
        cpassword.setCustomValidity("Passwords Don't Match");
        document.getElementsByName("password")[0].style.boxShadow = "0 0 35px #FF0000";
        document.getElementsByName("cpassword")[0].style.boxShadow = "0 0 35px #FF0000";
        document.getElementsByName("password")[0].style.border = "3px solid #FF0000";
        document.getElementsByName("cpassword")[0].style.border = "3px solid #FF0000";




    } else {
        cpassword.setCustomValidity('');
        document.getElementsByName("password")[0].style.boxShadow = "0 0 35px #74ebe6";
        document.getElementsByName("cpassword")[0].style.boxShadow = "0 0 35px #74ebe6";
        document.getElementsByName("password")[0].style.border = "3px solid #74ebe6";
        document.getElementsByName("cpassword")[0].style.border = "3px solid #74ebe6";

    }
};
