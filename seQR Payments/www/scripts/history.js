﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.

// all global variables for information on the user and transactions
var getName;
var getDate1;
var getPrice;
var data;
var username;
var balance;
var table;
var sum;
var date;
var time;
var price;
var transactions;
var row;
var cell1;
var cell2;
var cell3;
var count = 1;
var currentUserId;
var curBalance;
var user;
var curUsername;
var countTransaction=0;
(function () {
    "use strict";


    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);

        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
        var greet = document.getElementById("userGreet");
        //data = window.localStorage.getItem(window.localStorage.getItem("currentUser"));
        //username = data.substring(0, data.indexOf(":"));
        //balance = window.localStorage.getItem("balance");
        user = window.localStorage.getItem("currentUser");

        var db = openDatabase('seQRDatabase', '1.0', 'seQRDatabase', '2*1024*1024');
        db.transaction(function (tx) {
            tx.executeSql('SELECT userid FROM USERS WHERE email =\"' + user + '\"', [], function (tx, results) {
                var len = results.rows.length, i;
                for (i = 0; i < len; i++) {
                    currentUserId = results.rows.item(i).userid;
                }


                tx.executeSql('SELECT balance FROM USERS WHERE userid=\"' + currentUserId + '\"', [], function (tx, results) {
                    var length = results.rows.length, i;
                    for (i = 0; i < length; i++) {
                        curBalance = results.rows.item(i).balance;
                    }
                });
                tx.executeSql('SELECT username FROM USERS WHERE userid=\"' + currentUserId + '\"', [], function (tx, results) {
                    var len2 = results.rows.length, i;
                    for (i = 0; i < len2; i++) {
                        curUsername = results.rows.item(i).username;
                    }
                    greet.innerHTML = "welcome " + curUsername + ", here are your recent transactions:";
                });
                table = document.getElementById("transactionTable");
                sum = 0;
                
                tx.executeSql('SELECT date,name,price FROM TRANSACTIONS WHERE userid=\"' + currentUserId + '\"', [], function (tx, results) {
                    var leng = results.rows.length, i;
                    for(i=0;i<leng;i++)
                    {
                        getDate1 = results.rows.item(i).date;
                        getName = results.rows.item(i).name;
                        getPrice = results.rows.item(i).price;
                        countTransaction = countTransaction + 1;
                        generateTransactions();
                        
                    }
                    generateTransactions2();
                });

            });

        });
        
        // generate the table with transactions
        //transactions = window.localStorage.getItem("transactions:" + window.localStorage.getItem("currentUser"));
        
       
        

        

    };

    function generateTransactions()
    {
        
        // remove first : from transactions
        //transactions = transactions.substr(1);
        date = getDate1;
        // remove date and next :
        //transactions = transactions.substr(transactions.indexOf(":") + 1);
        name = getName;
        // remove name and next :
        //transactions = transactions.substr(transactions.indexOf(":") + 1);
        price = getPrice;
        // finally consume last : of this record, repeating for next record after table insertion
        //transactions = transactions.substr(transactions.indexOf(":") + 1);
        generateRow(date, name, price);
    } 
           
    function generateTransactions2(){
        // final sum of all the data in final table entry
        row = table.insertRow();

        if (parseFloat(sum) > 0)
        {
            row.style.color = "Green";
        }
        else
        {
            row.style.color = "Red";
        }

        cell1 = row.insertCell();
        cell2 = row.insertCell();
        cell3 = row.insertCell();
        cell1.innerHTML = "<b>" + date + "</b>";
        cell2.innerHTML = "<b>Current Transaction Balance</b>";
        cell3.innerHTML = "<b>" + sum.toFixed(2) + "</b>";
        row = table.insertRow();
        cell1 = row.insertCell();
        cell2 = row.insertCell();
        cell3 = row.insertCell();
        cell1.innerHTML = "<b>" + date + "</b>";
        cell2.innerHTML = "<b>Account Balance Now</b>";
        cell3.innerHTML = "<b>" + curBalance + "</b>";


    }

    function generateRow(tDate, tName, tPrice)
    {
        count = count * -1;
        row = table.insertRow();
        if (parseFloat(tPrice) > 0)
        {
            row.style.color = "Green";  
        }
        else
        {
            row.style.color = "Red"; 
        }
        cell1 = row.insertCell();
        cell2 = row.insertCell();
        cell3 = row.insertCell();
        cell1.innerHTML = tDate;
        cell2.innerHTML = tName;
        cell3.innerHTML = tPrice;
        sum += parseFloat(tPrice);
        
        if (count == 1)
        {
            row.style.backgroundColor = "Grey";
        }
        else
        {
            row.style.backgroundColor = "Gainsboro";
        }
    }


    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };


})();

