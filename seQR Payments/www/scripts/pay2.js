﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
var currentUserId;
var curBalance;
var user;
var curUsername;
(function () {
    "use strict";
    var amount;
    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);

        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
        amount = document.location.search.substr(document.location.search.indexOf("=") + 1);
        user = window.localStorage.getItem("currentUser");
        
        var db = openDatabase('seQRDatabase', '1.0', 'seQRDatabase', '2*1024*1024');
        db.transaction(function (tx) {
            tx.executeSql('SELECT userid FROM USERS WHERE email =\"' + user + '\"', [], function (tx, results) {
                var len = results.rows.length, i;
                for (i = 0; i < len; i++) {
                    currentUserId = results.rows.item(i).userid;
                }


                tx.executeSql('SELECT balance FROM USERS WHERE userid=\"' + currentUserId + '\"', [], function (tx, results) {
                    var length = results.rows.length, i;
                    for (i = 0; i < length; i++) {
                        curBalance = results.rows.item(i).balance;
                    }
                });
                tx.executeSql('SELECT username FROM USERS WHERE userid=\"' + currentUserId + '\"', [], function (tx, results) {
                    var len2 = results.rows.length, i;
                    for(i=0;i<len2;i++)
                    {
                        curUsername = results.rows.item(i).username;
                    }
                });
            });
        });
        

        // not enough money in their account
        if (parseFloat(amount) > parseFloat(curBalance))
        {
            navigator.notification.alert("Sorry, you don't have enought credit to pay €" + amount + ", you only have a balance of €" + curBalance);
            window.location.href = "mainMenu.html";
        }
        else
        {
            navigator.notification.confirm("Do you confirm sending this amount? (€" + amount + ")", onConfirm, "Confirm Payment", ["I Confirm Payment", "Cancel Payment"]);
        }

        


        function onConfirm(buttonIndex) {
            if (buttonIndex == 1) {
                // parse from get form sent to this page when pay is filled out
                amount = document.location.search.substr(document.location.search.indexOf("=") + 1);
                //var user = window.localStorage.getItem("currentUser");
                //var data = window.localStorage.getItem(user);
                //alert(data);
                //var name = data.substring(0, data.indexOf(":"));
                // part of API text from BarCodeScanner available at https://github.com/phonegap/phonegap-plugin-barcodescanner
                cordova.plugins.barcodeScanner.encode(cordova.plugins.barcodeScanner.Encode.TEXT_TYPE, "Recieving" + ":" + curUsername + ":" + amount, function (success) {
                
                }, function (fail) {
                    navigator.notification.alert("Something went wrong encoding the QR Code, please try again"); 
                });
                var oldBalance = curBalance;
                var newBalance = parseFloat(oldBalance) - parseFloat(amount);
                //window.localStorage.setItem("balance", newBalance.toString());
                
                // setting new transaction to storage
                //var user = window.localStorage.getItem("currentUser");
                //var transactions = window.localStorage.getItem("transactions:" + user);
                // making date of the transaction

                var date = new Date();
                var dd = date.getDate();
                var mm = date.getMonth() + 1;
                var yyyy = date.getFullYear();

                if (dd < 10) {
                    dd = '0' + dd
                }

                if (mm < 10) {
                    mm = '0' + mm
                }

                date = dd + '/' + mm + '/' + yyyy;
                var newid = (new Date().getMilliseconds()).toString();
                var transactionName = "Payment QR Generated";
                var db = openDatabase('seQRDatabase', '1.0', 'seQRDatabase', '2*1024*1024');
                var amountToStore = "-" + amount.toString();
                db.transaction(function (tx) {
                    tx.executeSql('UPDATE USERS SET balance=\"' + newBalance + '\" WHERE userid=\"'+currentUserId+'\"');
                    tx.executeSql('INSERT INTO TRANSACTIONS(transactionid,userid,date,name,price) VALUES(?,?,?,?,?)', [newid, currentUserId, date, transactionName, amountToStore]);
                    alert("query ok");
                });
                //saving transaction to storage along with other transactions
                //window.localStorage.setItem("transactions:" + user, transactions + ":" + date + ":Payment QR Generated:-" + amount + ":");
                navigator.notification.alert("Payment made of €" + amount + " your new balance is €" + newBalance);

            }
        };

    };
    
    

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };

    
})();

/*
function scan() {
    cordova.plugins.barcodeScanner.scan(
function (result) {
    if (!result.cancelled) {
        // In this case we only want to process QR Codes
        if (result.format == "QR_CODE") {
            var value = result.text;
            // This is the retrieved content of the qr code
            console.log(value);
        } else {
            alert("Sorry, only qr codes this time ;)");
        }
    } else {
        alert("The user has dismissed the scan");
    }
},
    function (error) {
        alert("An error ocurred: " + error);
    }
);
}
*/

/*
cordova.plugins.barcodeScanner.scan(
        function (result) {
            if (!result.cancelled) {
                // In this case we only want to process QR Codes
                if (result.format == "QR_CODE") {
                    var value = result.text;
                    // This is the retrieved content of the qr code
                    console.log(value);
                } else {
                    alert("Sorry, only qr codes this time ;)");
                }
            } else {
                alert("The user has dismissed the scan");
            }
        },
            function (error) {
                alert("An error ocurred: " + error);
            }
        );
*/



/*
cordova.plugins.barcodeScanner.encode(barcodeScanner.Encode.TEXT_TYPE, "Exhaust", function (success) {
    alert("encode success: " + success);
}, function (fail) {
    alert("encoding failed: " + fail);
}
);
*/