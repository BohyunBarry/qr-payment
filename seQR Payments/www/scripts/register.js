﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.

(function () {
    "use strict";


    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);

        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
        

    };
  

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };


})();

function insertUser() {
    var db = openDatabase('seQRDatabase', '1.0', 'seQRDatabase', '2*1024*1024');
    
    var email = document.getElementsByName("email")[0].value;
    var username = document.getElementsByName("username")[0].value;
    var password = document.getElementsByName("password")[0].value;
    var cpassword = document.getElementsByName("cpassword")[0].value;

    if (window.localStorage.getItem(email) === null && password == cpassword) {

        // setting up user account and associated transactions for this account
        //window.localStorage.setItem(email, username + ":" + password + ":0");
        //window.localStorage.setItem("transactions:" + email, "");

        //generate new unique id

        var newid = (new Date().getMilliseconds()).toString();
        var balance = 20;
        
        // showing back the new record ID for the newly created user
        alert(newid);
        db.transaction(function (tx) {
            // statement to set up the database, not needed when live on microsoft azure, here instead of index to reduce processor and database overhead when app is loaded up
            // logically a user would only be running this script when they were signing up for using our application
            tx.executeSql('CREATE TABLE IF NOT EXISTS USERS(userid,email,username,password,balance)');
            tx.executeSql('CREATE TABLE IF NOT EXISTS CARDS(cardid,userid,cardnumber,year,month,cvc)');
            tx.executeSql('CREATE TABLE IF NOT EXISTS TRANSACTIONS(transactionid,userid,date,name,price)');

            tx.executeSql('INSERT INTO USERS (userid,email,username,password,balance) VALUES (?,?,?,?,?)', [newid, email, username, password, balance]);

        });

        navigator.notification.alert("Congrats, " + username + " you are now a member of SeQR payments");
    }

    else {
        navigator.notification.alert("This user already exists");
    }


};

// Validation code from: 
// http://codepen.io/diegoleme/pen/surIK

function validatePassword() {

    var password = document.getElementsByName("password")[0];
    var cpassword = document.getElementsByName("cpassword")[0];

    if (password.value != cpassword.value) {
        cpassword.setCustomValidity("Passwords Don't Match");
        document.getElementsByName("password")[0].style.boxShadow = "0 0 35px #FF0000";
        document.getElementsByName("cpassword")[0].style.boxShadow = "0 0 35px #FF0000";
        document.getElementsByName("password")[0].style.border = "3px solid #FF0000";
        document.getElementsByName("cpassword")[0].style.border = "3px solid #FF0000";




    } else {
        cpassword.setCustomValidity('');
        document.getElementsByName("password")[0].style.boxShadow = "0 0 35px #74ebe6";
        document.getElementsByName("cpassword")[0].style.boxShadow = "0 0 35px #74ebe6";
        document.getElementsByName("password")[0].style.border = "3px solid #74ebe6";
        document.getElementsByName("cpassword")[0].style.border = "3px solid #74ebe6";

    }
};


